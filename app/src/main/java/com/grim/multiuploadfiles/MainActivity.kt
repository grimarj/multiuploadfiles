package com.grim.multiuploadfiles

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class MainActivity : AppCompatActivity() {

    lateinit var adapter: RecyclerAdapter

    var id: Int = 0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val recycler:RecyclerView = findViewById(R.id.recycler)

        recycler.layoutManager = LinearLayoutManager(this)


        adapter = RecyclerAdapter()

        recycler.adapter = adapter

    }



    private fun addItem(){
        id++
        adapter.addItemToList(ItemsGus(id,"item", 0, 0))

    }



    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.main_menu, menu)

        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when(item.itemId){

            R.id.add ->{

                addItem()
                true
            }
            else -> super.onOptionsItemSelected(item)

        }

    }



}

data class ItemsGus (val id:Int , val name: String, var progress : Int, var status : Int)