package com.grim.multiuploadfiles


import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat


class TimeService:Service() {



    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private fun initNotification(){

        val builder = NotificationCompat.Builder(this, "channel_id")
            .setContentTitle("test")
            .setContentText("test content")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT);



        if (Build.VERSION.SDK_INT  >= Build.VERSION_CODES.O){

            val name = "gus"
            val descriptionText = "test channel"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("channel_id", name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)



        }



    }



    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {



        return START_STICKY

    }

}