package com.grim.multiuploadfiles


import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import java.util.concurrent.Executors



class RecyclerAdapter : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>(){


    var handler = Handler(Looper.myLooper()!!)

    var list = ArrayList<ItemsGus>()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {

        val mTitle:TextView
        val mSubtitle: TextView
        val mCheck : ImageView
        val mImageUpload : ImageView
        val mProgress: ProgressBar

        val btnRemove: ImageView


        init{
            mSubtitle = itemView.findViewById(R.id.subititle)
            mCheck = itemView.findViewById(R.id.check)

            mTitle = itemView.findViewById(R.id.title)
            mImageUpload = itemView.findViewById(R.id.upload)
            mProgress = itemView.findViewById(R.id.progress)
            btnRemove = itemView.findViewById(R.id.remove)

        }

        fun bind(item : ItemsGus){

            mTitle.text  = item.name
            mSubtitle.text = item.id.toString()

            //itemView.setOnClickListener(this)
            mImageUpload.setOnClickListener(this)
            btnRemove.setOnClickListener(this)


        }

        override fun onClick(v: View?) {

            //Log.w("status click position", adapter.list[adapterPosition].status.toString())

            //adapter.startThread(absoluteAdapterPosition)

            if (v?.id == R.id.remove){

                removeItem(absoluteAdapterPosition)

            }else if (v?.id == R.id.upload ){

                sendingFile(absoluteAdapterPosition, list[absoluteAdapterPosition])
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        val inflate = LayoutInflater.from(parent.context).inflate(R.layout.row, parent, false)

        return ViewHolder(inflate)
    }


    private fun sendingFile(position: Int, element: ItemsGus){

        element.status = 1


        notifyItemChanged(position)

        val executor = Executors.newSingleThreadExecutor()
        executor.execute{

            Thread.sleep(2000)
            handler.post {

                element.status = 2

                val position2 = getIndexPosition(element)
                notifyItemChanged(position2)


            }

        }
    }

    private fun getIndexPosition(item: ItemsGus): Int{

        for ((index, value) in list.withIndex()){
            if (item.id == value.id) return index
        }

        return -1
    }


    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val gus:ItemsGus = list[position]
        holder.bind(gus)


        when(gus.status){


            0 -> {
                holder.mImageUpload.visibility = View.VISIBLE
                holder.mCheck.visibility = View.INVISIBLE
                holder.mProgress.visibility = View.GONE
            }

            1 -> {

                holder.mProgress.visibility = View.VISIBLE
                holder.mImageUpload.visibility = View.GONE
                holder.mCheck.visibility = View.INVISIBLE

            }
            2 -> {

                holder.mImageUpload.visibility = View.GONE
                holder.mCheck.visibility = View.VISIBLE
                holder.mCheck.setImageResource(R.drawable.ic_check_circle_black_24dp)
                holder.mProgress.visibility = View.GONE
            }


        }




    }


    fun addItemToList(item : ItemsGus){

        list.add(item)
        notifyItemInserted(list.size)

    }

    fun removeItem(position: Int){

        list.removeAt(position)
        notifyItemRemoved(position)

    }


    class DifflutiLoads(var oldList: List<ItemsGus>, var newList: List<ItemsGus> ) : DiffUtil.Callback(){
        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].name == newList[newItemPosition].name
        }



    }

}

